# 1CoinH api

Backend API for the 1CoinH application (work in progress)

This project is built with [Express](https://expressjs.com/) and uses [sequelize](https://sequelize.org/) as Database ORM.


> *Note: you will need to have [Node.js](https://nodejs.org) and [git](https://git-scm.com/) installed.*


## Get started
Clone this repository...

```bash
git clone git@gitlab.com:1coinh/1coinh-api.git
```

or, if cloning with the above url which requires ssh doesn't work:
```bash
git clone https://gitlab.com/1coinh/1coinh-api.git
```

This will create a directory called `1coinh-api` cd into this directory and install the dependencies...

```bash
cd 1coinh-api
npm install
```

...then start:

```bash
npm start
```

Navigate to [localhost:8081](http://localhost:8081). You should see a welcome page.

By default, the server will only respond to requests from localhost.

We recommend using [Visual Studio Code](https://code.visualstudio.com/) for development.
