/**
 * Dependencies.
 */
var express = require('express');
var path = require('path');
const Sequelize = require('sequelize');
const fs = require('fs');
const {
    restore
} = require('./functions');

/**
 * Database initialization.
 */
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './data/database.sqlite'
});

// Profile table
const Profile = sequelize.define(
    'profiles', {
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true
        },
        name: Sequelize.STRING,
        birthday: Sequelize.STRING,
        features: Sequelize.STRING,
        gender: Sequelize.STRING,
        bodylength: Sequelize.STRING,
        bodylengthunit: Sequelize.STRING,
        lefteye: Sequelize.STRING,
        righteye: Sequelize.STRING,
        haircolor: Sequelize.STRING,
        hash: Sequelize.TEXT,
        IDCardPicture: Sequelize.STRING
    }
);

/**
 * Initialize the database,
 * creates ./data/database.sqlite if it doesn't exists
 * and creates the tables.
 */
sequelize.sync({
    force: false
});

var app = express();
app.get('/', function(req, res) {
    res.send("Hello world!");
});

app.get('/test', function(req, res) {
    fs.readFile('./tests/fixtures/backup.txt', 'utf8', function(err, data) {
        if (err) {
            res.send(err);
        }
        // process data
        const profile = restore(data)
        // the profile is returned as json, next step is to store it in the database first
        res.send(profile)
    });

})

/**
 * The Web application, listening on http://localhost:8081 
 */
app.listen(8081);
console.log('1coinh API started at http://localhost:8081');